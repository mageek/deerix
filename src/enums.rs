use std::fmt;

use serde::Serialize;

#[allow(non_camel_case_types)]
#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum TrackFormats {
    FLAC,
    MP3_320,
    MP3_128,
}

impl fmt::Display for TrackFormats {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            TrackFormats::FLAC => write!(f, "FLAC"),
            TrackFormats::MP3_320 => write!(f, "MP3_320"),
            TrackFormats::MP3_128 => write!(f, "MP3_128"),
        }
    }
}

pub fn track_formats_to_string(track_string: &str) -> Result<TrackFormats, String> {
    Ok(match track_string {
        "MP3_128" => TrackFormats::MP3_128,
        "MP3_320" => TrackFormats::MP3_320,
        "FLAC" => TrackFormats::FLAC,
        _ => return Err(format!("Failed to convert {track_string} to TrackFormat enum, allowed values MP3_128, MP3_320, FLAC.")),
    })
}

#[derive(Debug, Serialize)]
#[serde(tag = "method")]
pub enum DeezerApiRequest {
    #[serde(rename = "deezer.getUserData")]
    UserData,

    #[serde(rename = "song.getData")]
    SongData {
        #[serde(rename = "sng_id")]
        id: u64,
    },
}
