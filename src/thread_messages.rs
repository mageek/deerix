use std::thread::sleep;
use std::time::Duration;

use rand::{thread_rng, Rng};

#[allow(clippy::enum_variant_names)]
#[derive(Debug)]
pub enum Messages {
    // Artist, Comment, Downloaded songs number, Not Downloaded Items
    DownloadedArtist(u32, String, u32, NotDownloadedItems),
    // AlbumId, Comment, Downloaded tracks, Not Downloaded Items
    DownloadedAlbums(u32, Option<String>, Vec<u32>, NotDownloadedItems),
    // TrackId, Comment
    DownloadedTrack(u32, NotDownloadedItems),
}

pub fn thread_sleep(min: f32, max: f32) {
    let rand_value = thread_rng().gen_range(min..max);
    sleep(Duration::from_millis((rand_value * 1000.0) as u64));
}

#[derive(Debug, Default)]
pub struct NotDownloadedItems {
    pub albums: Vec<(u32, String)>,
    pub songs: Vec<(u32, String)>,
    pub ignored_songs: Vec<(u32, String)>,
}

#[derive(Default)]
pub struct DownloadedItems {
    pub albums: Vec<u32>,
    pub songs: Vec<u32>,
    // pub artists: Vec<u32>, // Not needed now
}
