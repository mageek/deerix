use std::sync::atomic::{AtomicU32, Ordering};
use std::{process, thread};

use crossbeam_channel::{unbounded, Receiver, Sender};
use rayon::prelude::*;
use reqwest::blocking::Client;

use crate::api::get_track_info;
use crate::config::{BaseConfig, Config};
use crate::downloading::{collect_song_data_from_collected_track, generate_url_and_download_it, get_not_downloaded_songs_from_errors};
use crate::thread_messages::Messages;

pub fn downloading_tracks(base_config: &BaseConfig, config: &mut Config, client: &mut Client, ctrl_c_receiver: Receiver<()>, download_not_downloaded_tracks: bool) {
    if download_not_downloaded_tracks {
        println!("Stared checking for tracks that failed to download.");
    } else {
        println!("Stared checking for tracks to download.");
    }

    let (tx, rx) = unbounded::<Messages>();

    let client_cloned = client.clone();
    let base_config = base_config.clone();
    let tracks_to_download = if download_not_downloaded_tracks {
        config.tracks_not_downloaded.clone()
    } else {
        config.tracks_to_download.clone()
    };
    let downloading_thread = thread::spawn(move || {
        let track_counter = AtomicU32::new(0);
        let track_number = tracks_to_download.len() as u32;

        tracks_to_download
            .into_par_iter()
            .map(|(track_idx, comment)| {
                let mut client = client_cloned.clone();
                let tx = tx.clone();
                let ctrl_c_receiver = ctrl_c_receiver.clone();
                download_single_track(
                    &mut client,
                    &base_config,
                    track_counter.fetch_add(1, Ordering::Acquire),
                    track_idx,
                    track_number,
                    &comment,
                    &tx,
                    &ctrl_c_receiver,
                );
            })
            .collect::<Vec<_>>();
    });

    while let Ok(value) = rx.recv() {
        // dbg!(&value);
        match value {
            Messages::DownloadedTrack(track_id, not_downloaded) => {
                config.tracks_not_downloaded.remove(&track_id);
                config.tracks_to_download.remove(&track_id);
                config.ignored_songs.remove(&track_id);

                for (track_id, comment) in not_downloaded.songs {
                    config.tracks_not_downloaded.insert(track_id, comment);
                }
                for (track_id, comment) in not_downloaded.ignored_songs {
                    config.ignored_songs.insert(track_id, comment);
                }

                config.save_to_file();
                println!("Completed checking of track {track_id}");
            }
            _ => {
                panic!("Not supported message {value:?}")
            }
        }
    }

    downloading_thread.join().unwrap();
    if download_not_downloaded_tracks {
        println!("Completed checking for tracks that failed to download.");
    } else {
        println!("Completed checking for tracks to download.");
    }
}

fn download_single_track(client: &mut Client, base_config: &BaseConfig, idx: u32, track_id: u32, tracks_number: u32, comment: &str, tx: &Sender<Messages>, crx: &Receiver<()>) {
    let mut all_errors = Vec::new();
    match get_track_info(client, base_config, track_id) {
        Ok(full_track_struct) => {
            if crx.try_recv().is_ok() {
                println!("Closing cleanly app.");
                process::exit(0);
            }
            println!("_____ Downloading track {:5}/{:<5} ({:<20})", idx + 1, tracks_number, comment);

            let song_data = collect_song_data_from_collected_track(&full_track_struct);
            if let Err(err) = generate_url_and_download_it(track_id, &song_data, base_config, client) {
                all_errors.push(err);
            }
        }
        Err(e) => {
            eprintln!("{e}");
            all_errors.push(e);
        }
    }

    // Completed downloading

    let not_downloaded = get_not_downloaded_songs_from_errors(all_errors);
    tx.send(Messages::DownloadedTrack(track_id, not_downloaded)).unwrap();
}
