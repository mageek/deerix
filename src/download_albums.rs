use std::sync::atomic::{AtomicU32, Ordering};
use std::{process, thread};

use crossbeam_channel::{unbounded, Receiver, Sender};
use rayon::prelude::*;
use reqwest::blocking::Client;

use crate::api::{get_master_in_struct_track_info, get_track_info};
use crate::config::{BaseConfig, Config};
use crate::downloading::{collect_song_data_from_collected_track, generate_url_and_download_it, get_not_downloaded_songs_from_errors};
use crate::thread_messages::Messages;

pub fn downloading_albums(base_config: &BaseConfig, config: &mut Config, client: &mut Client, ctrl_c_receiver: Receiver<()>, download_not_downloaded_albums: bool) {
    if download_not_downloaded_albums {
        println!("Stared checking for albums that failed to download.");
    } else {
        println!("Stared checking for albums to download.");
    }

    let (tx, rx) = unbounded::<Messages>();

    let albums_to_download = if download_not_downloaded_albums {
        config.albums_not_downloaded.clone()
    } else {
        config.albums_to_download.clone()
    };
    let client_cloned = client.clone();
    let base_config = base_config.clone();
    let downloading_thread = thread::spawn(move || {
        let album_counter = AtomicU32::new(0);
        let albums_number = albums_to_download.len() as u32;

        albums_to_download
            .into_par_iter()
            .map(|(album_idx, comment)| {
                let mut client = client_cloned.clone();
                let tx = tx.clone();
                let ctrl_c_receiver = ctrl_c_receiver.clone();
                download_single_album_songs(
                    &mut client,
                    &base_config,
                    album_counter.fetch_add(1, Ordering::Acquire),
                    albums_number,
                    album_idx,
                    &comment,
                    &tx,
                    &ctrl_c_receiver,
                );
            })
            .collect::<Vec<_>>();
    });

    while let Ok(value) = rx.recv() {
        // dbg!(&value);
        match value {
            Messages::DownloadedAlbums(album_id, comment, downloaded_tracks, not_downloaded) => {
                if let Some(comment) = comment {
                    config.albums_to_download.remove(&album_id);
                    config.albums_not_downloaded.insert(album_id, comment);
                } else {
                    config.albums_to_download.remove(&album_id);
                    config.albums_not_downloaded.remove(&album_id);
                }

                for track_id in downloaded_tracks {
                    config.tracks_to_download.remove(&track_id);
                }
                for (track_id, comment) in not_downloaded.songs {
                    config.tracks_to_download.remove(&track_id);
                    config.tracks_not_downloaded.insert(track_id, comment);
                }
                for (track_id, comment) in not_downloaded.ignored_songs {
                    config.tracks_to_download.remove(&track_id);
                    config.tracks_not_downloaded.remove(&track_id);
                    config.ignored_songs.insert(track_id, comment);
                }

                config.save_to_file();
                println!("Completed checking of album {album_id}");
            }
            _ => {
                panic!("Message type not supported")
            }
        }
    }

    downloading_thread.join().unwrap();
    if download_not_downloaded_albums {
        println!("Completed checking for albums that failed to download.");
    } else {
        println!("Completed checking for albums to download.");
    }
}

fn download_single_album_songs(
    client: &mut Client,
    base_config: &BaseConfig,
    idx: u32,
    albums_number: u32,
    album_id: u32,
    comment: &str,
    tx: &Sender<Messages>,
    crx: &Receiver<()>,
) {
    let mut all_errors = Vec::new();
    let downloaded_tracks = Vec::new();
    let mut album_error = None;
    println!("_____ Downloading album {:5}/{:<5} ({:<10})", idx + 1, albums_number, comment);

    match get_master_in_struct_track_info(client, base_config, album_id) {
        Ok(master_track_struct) => {
            let tracks_number = master_track_struct.data.len();
            for (track_idx, track_struct_in_album) in master_track_struct.data.into_iter().enumerate() {
                let track_id = track_struct_in_album.id;
                if crx.try_recv().is_ok() {
                    println!("Closing cleanly app.");
                    process::exit(0);
                }
                println!(
                    "_____ Downloading track {:5}/{:<5} ({:10}) from album {:10}",
                    track_idx + 1,
                    tracks_number,
                    track_id,
                    album_id
                );
                match get_track_info(client, base_config, track_struct_in_album.id) {
                    Ok(full_track_struct) => {
                        let song_data = collect_song_data_from_collected_track(&full_track_struct);
                        if let Err(err) = generate_url_and_download_it(track_struct_in_album.id, &song_data, base_config, client) {
                            all_errors.push(err);
                        }
                    }
                    Err(e) => {
                        eprintln!("{e}");
                        all_errors.push(e);
                    }
                }
            }
        }
        Err(e) => {
            eprintln!("{e}");
            album_error = Some(e.to_string());
            all_errors.push(e);
        }
    };

    // Completed downloading
    let not_downloaded = get_not_downloaded_songs_from_errors(all_errors);
    tx.send(Messages::DownloadedAlbums(album_id, album_error, downloaded_tracks, not_downloaded)).unwrap();
    //, ignored_songs, not_downloaded_songs)).unwrap();
}
